import { shallowMount } from "@vue/test-utils";
import ProgressRing from "@/components/progress_ring.vue";

let wrapper;

const defaultProps = {
  daysInMilestone: 100,
  daysPassedInMilestone: 14,
  issueCompletion: 25,
};
const colorClasses = {
  green: "text-green-600",
  orange: "text-orange-600",
};
const createWrapper = (props = {}) => {
  wrapper = shallowMount(ProgressRing, {
    propsData: { ...defaultProps, ...props },
  });
};

describe("Progress Ring", () => {
  afterEach(() => {
    wrapper.destroy();
  });

  describe("issue completion ring", () => {
    describe.each`
      issueCompletion | percentage
      ${0}            | ${"0"}
      ${10}           | ${"10"}
      ${100}          | ${"100"}
      ${200}          | ${"100"}
    `("with $progress% progress", ({ issueCompletion, percentage }) => {
      beforeEach(() => {
        createWrapper({ issueCompletion });
      });

      // Note: we check for the css variable here as it's rendered as
      // an SVG offset and that would be a nightmare to test
      it(`renders the ring with a length of ${percentage}`, () => {
        expect(
          getComputedStyle(
            wrapper.find(".progress-ring").element
          ).getPropertyValue("--completed")
        ).toEqual(percentage);
      });
    });
  });

  describe("time passed ring", () => {
    describe.each`
      daysInMilestone | daysPassedInMilestone | percentage
      ${100}          | ${0}                  | ${"0"}
      ${100}          | ${10}                 | ${"10"}
      ${100}          | ${100}                | ${"100"}
      ${200}          | ${1}                  | ${"0.5"}
      ${28}           | ${14}                 | ${"50"}
      ${1}            | ${2}                  | ${"100"}
      ${0}            | ${1}                  | ${"100"}
    `(
      "with $daysLeftInMilestone days left in a $daysInMilestone day milestone",
      ({ daysInMilestone, daysPassedInMilestone, percentage }) => {
        beforeEach(() => {
          createWrapper({ daysInMilestone, daysPassedInMilestone });
        });

        // Note: we check for the css variable here as it's rendered as
        // an SVG offset and that would be a nightmare to test
        it(`renders the ring with a length of ${percentage}`, () => {
          expect(
            getComputedStyle(
              wrapper.find(".progress-ring").element
            ).getPropertyValue("--time-passed")
          ).toEqual(percentage);
        });
      }
    );
  });

  describe("ring colors", () => {
    describe.each`
      issueCompletion | daysPassedInMilestone | color
      ${100}          | ${0}                  | ${"green"}
      ${90}           | ${10}                 | ${"green"}
      ${90}           | ${90}                 | ${"orange"}
      ${10}           | ${90}                 | ${"orange"}
      ${0}            | ${100}                | ${"orange"}
    `(
      "with $issueCompletion% of issues completed and $daysPassedInMilestone% of the milestone passed",
      ({ issueCompletion, daysPassedInMilestone, color }) => {
        beforeEach(() => {
          createWrapper({ issueCompletion, daysPassedInMilestone });
        });

        it(`renders the ${color} ring for issue completion`, () => {
          expect(wrapper.find(".completed").classes()).toContain(
            colorClasses[color]
          );
        });
      }
    );
  });
});
