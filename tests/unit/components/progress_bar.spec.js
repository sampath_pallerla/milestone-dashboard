import { shallowMount } from "@vue/test-utils";
import ProgressBar from "@/components/progress_bar.vue";

let wrapper;

const defaultProps = { progress: 0 };
const createWrapper = (props = {}) => {
  wrapper = shallowMount(ProgressBar, {
    propsData: { ...defaultProps, ...props },
  });
};

describe("Progress Bar", () => {
  afterEach(() => {
    wrapper.destroy();
  });

  describe.each`
    progress | percentage
    ${0}     | ${"0%"}
    ${10}    | ${"10%"}
    ${100}   | ${"100%"}
    ${200}   | ${"100%"}
  `("with $progress% progress", ({ progress, percentage }) => {
    beforeEach(() => {
      createWrapper({ progress });
    });

    it(`renders the bar with a width of ${percentage}`, () => {
      expect(wrapper.find(".progress").element.style.width).toEqual(percentage);
    });
  });
});
