import { shallowMount } from "@vue/test-utils";
import IssueBody from "@/components/issue_body.vue";
import ProgressBar from "@/components/progress_bar.vue";
import { WORKFLOW_LABEL_PREFIX } from "@/constants";

let wrapper;

const defaultProps = {
  iid: "1",
  completion: 10,
  title: "Issue Title",
  labels: [],
  webUrl: "https://gitlab.com",
};

const createWrapper = (props = {}) => {
  wrapper = shallowMount(IssueBody, {
    propsData: {
      ...defaultProps,
      ...props,
    },
  });
};

const findWorkflowBadge = () => wrapper.find(".workflow-badge");

describe("Issue Body", () => {
  afterEach(() => {
    wrapper.destroy();
  });

  describe("with an in-progress issue", () => {
    beforeEach(() => {
      createWrapper();
    });

    it("should not fade out the issue", () => {
      expect(wrapper.classes()).not.toContain("opacity-25");
    });

    it("should pass the completion prop to the progress bar", () => {
      expect(wrapper.find(ProgressBar).props().progress).toEqual(
        defaultProps.completion
      );
    });

    it("should render the iid", () => {
      expect(wrapper.find(".iid").text()).toEqual(defaultProps.iid);
    });

    it("should render the issue link", () => {
      expect(wrapper.attributes().href).toEqual(defaultProps.webUrl);
    });

    it("should render the title", () => {
      expect(wrapper.find("p").text()).toEqual(defaultProps.title);
    });

    it("should not render the workflow label", () => {
      expect(findWorkflowBadge().exists()).toBe(false);
    });
  });

  describe("with a completed issue", () => {
    beforeEach(() => {
      createWrapper({ completion: 100 });
    });

    it("should fade out the issue", () => {
      expect(wrapper.classes()).toContain("opacity-25");
    });
  });

  describe("with a completion above 100", () => {
    beforeEach(() => {
      createWrapper({ completion: 999 });
    });

    it("should fade out the issue", () => {
      expect(wrapper.classes()).toContain("opacity-25");
    });
  });

  describe("with a workflow label", () => {
    const WORKFLOW_LABEL = "in dev";

    beforeEach(() => {
      createWrapper({
        labels: [{ title: `${WORKFLOW_LABEL_PREFIX}${WORKFLOW_LABEL}` }],
      });
    });

    it("should render the workflow badge", () => {
      expect(findWorkflowBadge().exists()).toBe(true);
      expect(findWorkflowBadge().text()).toEqual(WORKFLOW_LABEL);
    });
  });
});
