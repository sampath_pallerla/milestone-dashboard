import getMilestoneStartDate from "@/utils/get_milestone_start_date";

describe("getMilestoneStartDate", () => {
  it.each`
    milestone  | endYear | endMonth
    ${"12.10"} | ${2020} | ${"March"}
    ${"13.0"}  | ${2020} | ${"April"}
    ${"13.1"}  | ${2020} | ${"May"}
    ${"13.7"}  | ${2020} | ${"November"}
    ${"13.8"}  | ${2020} | ${"December"}
    ${"14.0"}  | ${2021} | ${"April"}
    ${"14.10"} | ${2022} | ${"February"}
  `(
    "should get $endMonth $endYear for the $milestone milestone",
    ({ milestone, endYear, endMonth }) => {
      const result = getMilestoneStartDate(milestone);

      expect(result.toLocaleString("en-US", { month: "long" })).toEqual(
        endMonth
      );
      expect(result.getFullYear()).toEqual(endYear);
      expect(result.getDate()).toEqual(18);
    }
  );
});
