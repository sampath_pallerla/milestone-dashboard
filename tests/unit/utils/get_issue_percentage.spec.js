import getIssuePercentage from "@/utils/get_issue_percentage";
import generateIssue from "../../utils/generate_issue";

const generateLabels = (labels = []) => ({
  nodes: labels.map((title) => ({ title })),
});

describe("getIssuePercentage", () => {
  it("should return 100 for a closed issue", () => {
    const issue = generateIssue(0, { state: "closed" });
    const result = getIssuePercentage(issue);

    expect(result).toEqual(100);
  });

  it("should return 0 with no workflow labels", () => {
    const issue = generateIssue(0);
    const result = getIssuePercentage(issue);

    expect(result).toEqual(0);
  });

  it.each`
    label                      | percentage
    ${"ready for development"} | ${0}
    ${"blocked"}               | ${0}
    ${"In dev"}                | ${20}
    ${"In review"}             | ${60}
    ${"verification"}          | ${90}
  `(
    "should return $percentage with the workflow::$label label",
    ({ label, percentage }) => {
      const issue = generateIssue(0, {
        labels: generateLabels([`workflow::${label}`]),
      });

      expect(getIssuePercentage(issue)).toEqual(percentage);
    }
  );
});
