process.env.TZ = "GMT";
const reporters = ['default'];
let collectCoverage = false;

if (process.env.CI){
  reporters.push(['jest-junit', { outputName: './junit_jest.xml' }])
  collectCoverage = true;
}

module.exports = {
  preset: "@vue/cli-plugin-unit-jest",
  collectCoverage,
  collectCoverageFrom: ["src/**/*.{js,vue}"],
  coverageReporters: ["text", "text-summary"],
  moduleFileExtensions: ["js", "json", "vue"],
  moduleNameMapper: {
    "@/(.*)$": "<rootDir>/src/$1",
  },
  reporters
};
