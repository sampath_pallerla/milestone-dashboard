// WORKFLOW_LABELS
export const WORKFLOW_LABEL_PREFIX = "workflow::";
export const WORKFLOW_BLOCKED = `${WORKFLOW_LABEL_PREFIX}blocked`;
export const WORKFLOW_IN_DEV = `${WORKFLOW_LABEL_PREFIX}In dev`;
export const WORKFLOW_PLANNING = `${WORKFLOW_LABEL_PREFIX}planning breakdown`;
export const WORKFLOW_REVIEW = `${WORKFLOW_LABEL_PREFIX}In review`;
export const WORKFLOW_VERIFICATION = `${WORKFLOW_LABEL_PREFIX}verification`;
